clearvars
close all
clc


set(0,'defaultAxesFontSize',12)

global nb_fig;
nb_fig=0;
global colors;
global ream;
ream.xval=[];
ream.yval=[];
colors={'r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m'};

global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';

particleName = 'electron';

%% G4O1

plot_mul_fact_and_ava_times('REAM',particleName);
plot_mul_fact_and_ava_times('GRRR',particleName);
plot_mul_fact_and_ava_times('GEANT4O1',particleName);
plot_mul_fact_and_ava_times('GEANT4O4',particleName);

pause(0.1)
figure(10)
pause(0.1)

if contains(particleName,'electron')
    legend('REAM','GRRR','GEANT4O1','GEANT4O4')
else
    legend('REAM','GEANT4O1','GEANT4O4')
end

set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
figname = ['./generated_figures/time_to_steady_state/comparison/time_steady_state_compa_' particleName];
axis tight
savefig([figname '.fig'])
print(figname,'-depsc')



%%

function plot_mul_fact_and_ava_times(codename,particleName)
global path_to_processed_files;
global nb_fig;
global colors;
global ream;

if (contains(codename,'GRRR') && contains(particleName,'photon'))
    return
end

if contains(codename,'REAM')
    smooth_fact = 0.7;
else
    smooth_fact = 0.1;
end

nb_fig = nb_fig + 1;

pause(0.1)
figure(nb_fig)
pause(0.1)

load([path_to_processed_files codename '_data.mat']);

stop_times = HEAP_data_RREA.stop_times;
record_times = HEAP_data_RREA.record_times;
fields = HEAP_data_RREA.field_list;



the_legend={};

for ii=1:12
    
    nb_seed = 200; % default value
    nb_seed_did_RA = 200; % number that did runaway, default value
    
    record_times = record_times(record_times <= stop_times(ii));
    
    mean_ener=[];
    
    the_legend{end+1} = ['E-field of $' num2str(fields(ii)/1e5) '.10^5$ V/m'];
    the_legend{end+1} = '';
    
    field = fields(ii);
    
    time = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).time;
    energies = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).energy;
    %     nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
    
    if length(energies)==0
        continue
    end
    
    nb_parts = 0;
    
    for jj = 1:length(record_times)
        eners_filtered = energies(time>=record_times(jj)*0.999 & time<=record_times(jj)*1.001);
        mean_ener(jj) = mean(eners_filtered);
        nb_parts = nb_parts + length(eners_filtered);
    end
    
    %     values2=max(mean_ener)-mean_ener;
    
    record_times(isnan(mean_ener)) = [];
    %     values2(isnan(values2)) = [];
    mean_ener(isnan(mean_ener)) = [];
    
    %     values2 = smooth(values2',smooth_fact,'moving');
    
    mean_ener_save = mean_ener;
    
    mean_ener = smooth(mean_ener',smooth_fact,'moving');
    
    semilogx(record_times',mean_ener',['-' colors{ii}])
    hold on
    semilogx(record_times',mean_ener_save',['+' colors{ii}])
    
    xlabel('time (ns)','interpreter','latex')
    ylabel(['mean ' particleName ' energy (keV)'],'interpreter','latex')
    
    grid on
    title(['Average ' particleName ' energy vs time, ' codename],'interpreter','latex')
    
    options = fitoptions('exp2', 'Lower', [-Inf 0 -Inf -Inf], 'Upper', [ Inf 0  Inf  Inf]);
    
    try
        ff = fit(record_times',mean_ener_save','exp2',options);
        time_to_steadyState(ii) = 5 * -1/ff.d;
    catch ME
        time_to_steadyState(ii)=NaN;
    end
    
    time_to_steadyState(ii)
    
    ci = confint(ff);
    error = 5 ./ (-ci(:,4));
    
    if contains(codename,'REAM') && ii >= 9
        nb_seed =  16;
    end
    
    if (contains(codename,'REAM') || contains(codename,'GEANT4O4') || contains(codename,'GEANT4O1')) && ii == 1
        nb_seed_did_RA =  3;
    end
    
    if (contains(codename,'REAM') || contains(codename,'GEANT4O4') || contains(codename,'GEANT4O1')) && ii == 2
        nb_seed_did_RA =  20;
    end
    
    if (contains(codename,'REAM') || contains(codename,'GEANT4O4') || contains(codename,'GEANT4O1')) && ii == 3
        nb_seed_did_RA =  100;
    end
    
    if contains(codename,'GRRR') && ii >= 8
        nb_seed =  20;
    end
    
    
    incert_fit = abs(error(2) - time_to_steadyState(ii)) / time_to_steadyState(ii);
    incert_seed = 1./sqrt(nb_seed);
    incert_seed_stats = 1. / sqrt(nb_parts);
    incert_nb_ra = 1. / sqrt(nb_seed_did_RA);
    
    incert = sqrt(incert_seed^2 + incert_seed_stats^2 + incert_fit^2 + incert_nb_ra^2);
    
    error_up(ii)   = incert * time_to_steadyState(ii);
    error_down(ii) = incert * time_to_steadyState(ii);
    
end

pause(0.1)
figure(nb_fig)
pause(0.1)

legend(the_legend,'Location','northeastoutside','interpreter','latex');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
axis tight

figname = ['./generated_figures/time_to_steady_state/average_photon_energy_evolution/mean_ener_' particleName '_time_' codename];

savefig([figname '.fig'])
print(figname,'-depsc')

pause(0.1)
figure(10)

subplot(2,2,[1:2])
pause(0.1)

ttd = time_to_steadyState;

errorbar(fields(1:12), ttd, error_down, error_up, '+-','Displayname',codename)
set(gca, 'YScale', 'log')
legend('show')
hold on

grid on
% grid on
ylabel({'time to steady state (ns)'},'interpreter','latex')
% xlabel('E-field (V/m)','interpreter','latex')
% axis([0.5e6 3e6 20 2100])
axis tight

subplot(2,2,[3:4])

if contains(codename,'REAM')
    ream.xval = fields(1:12);
    ream.yval = ttd;
    rela_diff=zeros(size(ream.xval));
else
rela_diff = (ttd - ream.yval) ./ ream.yval .* 100  ;
end

plot(ream.xval, rela_diff,'Displayname',codename)

hold on

ylabel('Relative difference with REAM (\%)','interpreter','latex')
xlabel('E-field (V/m)','interpreter','latex')
legend('show')

clear HEAP_data_RREA;

end



%% comparing all codes

% figure
% plot(GEANT4O1.mean_ener.field_value, GEANT4O1.mean_ener.time_to_steadyState)
% hold on
% plot(GEANT4O4.mean_ener.field_value, GEANT4O4.mean_ener.time_to_steadyState)
% plot(REAM.mean_ener.field_value, REAM.mean_ener.time_to_steadyState)
% plot(GRRR.mean_ener.field_value(1:length(GRRR.mean_ener.time_to_steadyState)), GRRR.mean_ener.time_to_steadyState)
%
% xlabel('E-field (V/m)')
% ylabel('Time to steady state (ns)')
% grid on
% title('comparison of "times to steady state VS E-field"')
% savefig('./generated_figures/time_to_steady_state_compa.fig')
%
%
% legend('GEANT4O1','GEANT4O4','REAM','GRRR')