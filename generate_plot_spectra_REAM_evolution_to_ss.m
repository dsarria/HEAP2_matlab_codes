clear all
close all
clc


set(0,'defaultAxesFontSize',12)

%% globals
global nb_fig;
nb_fig = 0;
global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';
global bins;
global bins2;
bins = logspace(log10(10),log10(80000),80);
bins2 = logspace(log10(10),log10(80000),30);


%% plot for all codes

% load([path_to_processed_files 'REAM' '_data.mat']);

plot_mul_fact_and_ava_times('REAM','electron')
plot_mul_fact_and_ava_times('GEANT4O1','electron')

global ener_evol;
ener_evol.ream=[];
ener_evol.geant=[];

figure
plot(ener_evol.ream)
hold on
plot(ener_evol.geant)
legend('REAM','GEANT4O1')

%%

function plot_mul_fact_and_ava_times(codename,particleName)
global path_to_processed_files;
global nb_fig;
global bins;
global bins2;
global ener_evol;


if contains(particleName,'electron')
    ipart=1;
    binss = bins;
    fig_nb0=0;
elseif contains(particleName,'photon')
    ipart=2;
    binss = bins;
    fig_nb0=14;
elseif contains(particleName,'positron')
    ipart=2;
    binss = bins2;
    fig_nb0=28;
end

fields = [6:2:28]*1e5 ;

nb_fig = nb_fig +1;

load([path_to_processed_files codename '_data.mat']);

record_times =  HEAP_data_RREA.record_times;

stop_times = HEAP_data_RREA.stop_times;

for ii=1:HEAP_data_RREA.nb_fields
    
    counter=-1;
    
    pause(0.1)
    figure(fig_nb0 + ii)
    pause(0.1)
    
    field = fields(ii);
    
    if contains(codename,'REAM')
        linestyle = 'o-';
    else
        linestyle = 's-';
    end
    
    time = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).time;
    energies = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).energy;
    nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
    
    stop_tim = stop_times(ii);
    
    for kk=1:length(record_times)
        
        record_time = record_times(kk);
        
        counter = counter +1 ;
        
        energies2 = energies(time >= record_time*0.999 & time <= record_time*1.001);
        
        if contains(codename,'REAM') && ii==9
            ener_evol.ream(kk)=mean(energies2);
        elseif contains(codename,'GEANT4') && ii==9
            ener_evol.geant(kk)=mean(energies2);
        end
        
        % plot only for times close to the stop time, and some of them
        if (abs(stop_tim-record_time)/stop_tim)>0.6 || mod(counter,2)~=0 || record_time>stop_tim
            continue; % skipping current iteration of for loop
        end

        
        [nn3,xout3] = histcounts(energies2,binss);
        nn3 = nn3 ./diff(xout3) ./ nb_seed;
        summ = sum(nn3(~isnan(nn3)));
        nn3 = nn3./sum(nn3);
        nn3(isnan(nn3))=0;
        
        centers = (xout3(1:end-1) + xout3(2:end))/2.;
        
        centers = centers(nn3~=0);
        nn3 = nn3(nn3~=0);
        
        nn3(end+1)=1e-8;
        centers(end+1)=centers(end);
        
        %         histogram('BinEdges',xout3,'BinCounts',nn3,'DisplayStyle','stairs','DisplayName',[codename ', ' num2str(record_time) ' ns'])
        plot(centers,nn3,linestyle,'DisplayName',[codename ', ' num2str(record_time) ' ns'])
        
        hold on
        
        xlabel('Energy (keV)','interpreter','latex')
        ylabel('counts per bin, per bin size (keV$^{-1}$), per detected particle','interpreter','latex')
        title([particleName ' spectra, E-field of ' num2str(field/1e5) '.$10^5$ V/m ; recorded at ' num2str(stop_tim) ' ns'],'interpreter','latex')
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        grid on
        
    end
    
    %     legend(the_legend.field{ipart,ii})
    legend('show')
    
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    
    figname = ['./generated_figures/spectra_comparison/time_record/spectra_compa_' particleName '_stopTimes_' num2str(field/1e5) 'e5'];
    %     pause(0.1)
    %     savefig([figname '.fig'])
    %     pause(0.1)
    %     print(figname,'-depsc')
    %     pause(0.1)
    
end



clear HEAP_data_RREA;

end











% %% comparing all codes
%
% figure
% plot(GEANT4O1.mean_ener.field_value, GEANT4O1.mul_fact.avalanche_times,'-+')
% hold on
% plot(GEANT4O4.mean_ener.field_value, GEANT4O4.mul_fact.avalanche_times,'-+')
% plot(REAM.mean_ener.field_value, REAM.mul_fact.avalanche_times,'-+')
% plot(GRRR.mean_ener.field_value(1:length(GRRR.mul_fact.avalanche_times)), GRRR.mul_fact.avalanche_times,'-+')
%
% grid on
% xlabel('Electric field (V/m)')
% ylabel('Avalanche time (ns)')
%
% legend('GEANT4O1','GEANT4O4','REAM','GRRR')
%
%
% %% functions
%
% function index = find_index(Efield, Efield_list)
%
% for ii=1:length(Efield_list)
%     if(Efield == Efield_list(ii))
%         index = ii;
%     end
% end
%
% end


