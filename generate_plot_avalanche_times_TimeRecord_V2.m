clear all
close all
clc


set(0,'defaultAxesFontSize',12)

%% globals
global nb_fig;
nb_fig = 0;
global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';
global avalanche_times_save;
avalanche_times_save={};

%% plot for all codes

plot_mul_fact_and_ava_times('GRRR')
plot_mul_fact_and_ava_times('REAM')
plot_mul_fact_and_ava_times('GEANT4O1')
plot_mul_fact_and_ava_times('GEANT4O4')


figure(10)
legend('GRRR','REAM','GEANT4O1','GEANT4O4')

set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);

title('Avalanche times comparison','interpreter','latex')

figname='./generated_figures/multiplication_factors/time_record_code_comparison/avalanche_time_compa';
savefig([figname '.fig'])
print(figname,'-depsc')

%%

function plot_mul_fact_and_ava_times(codename)
global path_to_processed_files;
global nb_fig;
global avalanche_times_save;

fields = [6:2:28]*1e5 ;

nb_fig = nb_fig +1;

load([path_to_processed_files codename '_data.mat']);

the_legend={};

for ii=1:length(fields)
    
    the_legend{end+1} = ['E-field of ' num2str(fields(ii)/1e5) '.$10^5$ V/m'];
    
    field = fields(ii);
    
    times = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.time;
    nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
    
    record_times = HEAP_data_RREA.record_times;
    
    for jj = 1:length(record_times)
        mult_fact(jj) = sum(times>=record_times(jj)*0.999 & times<=record_times(jj)*1.001) / nb_seed;
    end
    
    if (ii==12) && contains(codename,'GRRR') % removing specific point that leads to bad fit
        record_times = record_times(1:9);
        mult_fact = mult_fact(1:9);
        mult_fact
    end
    
    figure(nb_fig)
    semilogy(record_times,mult_fact,'-+')
    hold on
    
    record_times(mult_fact==0)=[];
    mult_fact(mult_fact==0)=[];
    
    ff = fit(record_times', mult_fact', 'exp1');
    avalanche_times(ii) = 1/ff.b;
    
    xlabel('Time (ns)','interpreter','latex')
    ylabel('Multiplciation factor','interpreter','latex')
    grid on
    title(['Avalanche times, ' codename],'interpreter','latex')
    
    
    
end

legend(the_legend,'Location','southeast','interpreter','latex');

set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);


avalanche_times_save{end+1} = avalanche_times;

figname=['./generated_figures/multiplication_factors/time_record_code_by_code/mult_fact_time_' codename];

savefig([figname '.fig'])
print(figname,'-depsc')



figure(10)
hold on
plot(fields(1:length(fields)), avalanche_times_save{end},'-+')
grid on
xlabel('E-Field (V/m)','interpreter','latex')
ylabel('Avalanche times (ns)','interpreter','latex')
hold on

clear HEAP_data_RREA;

end























% %% comparing all codes
%
% figure
% plot(GEANT4O1.mean_ener.field_value, GEANT4O1.mul_fact.avalanche_times,'-+')
% hold on
% plot(GEANT4O4.mean_ener.field_value, GEANT4O4.mul_fact.avalanche_times,'-+')
% plot(REAM.mean_ener.field_value, REAM.mul_fact.avalanche_times,'-+')
% plot(GRRR.mean_ener.field_value(1:length(GRRR.mul_fact.avalanche_times)), GRRR.mul_fact.avalanche_times,'-+')
%
% grid on
% xlabel('Electric field (V/m)')
% ylabel('Avalanche time (ns)')
%
% legend('GEANT4O1','GEANT4O4','REAM','GRRR')
%
%
% %% functions
%
% function index = find_index(Efield, Efield_list)
%
% for ii=1:length(Efield_list)
%     if(Efield == Efield_list(ii))
%         index = ii;
%     end
% end
%
% end


