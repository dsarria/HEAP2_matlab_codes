clearvars
close all
clc

set(0,'defaultAxesFontSize',12)

global velocity;
global radius;
velocity=[];
radius=[];

global nb_fields_save;
nb_fields_save = [];
global record_distances_save;
record_distances_save={};

global nb_fig;
nb_fig=0;
global colors;
colors={'r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m'};

global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';


load([path_to_processed_files 'GRRR' '_data.mat']);

%% G4O1

plot_mul_fact_and_ava_times('GRRR');
plot_mul_fact_and_ava_times('REAM');
plot_mul_fact_and_ava_times('GEANT4O1');
plot_mul_fact_and_ava_times('GEANT4O4');


plot_comparison();



%%

function plot_mul_fact_and_ava_times(codename)
global path_to_processed_files;
global nb_fig;
global velocity;
global radius;
global nb_fields_save;
global record_distances_save;

nb_shift_fig_for_rad = 10;

c_light = 2.99792458e8;

nb_fig = nb_fig + 1;

% nb_fig is also code number



particleName='electron';

load([path_to_processed_files codename '_data.mat']);

record_distances = HEAP_data_RREA.record_distances;

record_distances_save{nb_fig} = HEAP_data_RREA.record_distances;

fields = HEAP_data_RREA.field_list;

the_legend={};

nb_fields_save(nb_fig) = HEAP_data_RREA.nb_fields;

for i_field=1:12
    
    
    the_legend{end+1} = ['E-field of $' num2str(fields(i_field)/1e5) '.10^5$ V/m'];
    
    field = fields(i_field);
    
    xx = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.x;
    yy = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.y;
    zz = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.z;
    time = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).time;
    %     nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
    
    for i_dist = 1:length(record_distances)
        
        dist = record_distances(i_dist);
        
        to_be_kept = (zz >= 0.999*dist & zz <= 1.001*dist);
        
        time_t = time(to_be_kept);
        
        
        velocity(nb_fig,i_field,i_dist) = dist / mean(time_t*1e-9) / c_light;
        
        xx_t = xx(to_be_kept);
        yy_t = yy(to_be_kept);
        rr_t = sqrt(xx_t.^2 + yy_t.^2);
        radius(nb_fig,i_field,i_dist) = mean(rr_t);
    end
    
    
    
    rad = radius(nb_fig,i_field,:);
    vel = velocity(nb_fig,i_field,:);
    rad = rad(:);
    vel = vel(:);
    
    
    
    figure(nb_fig)
    
    plot(record_distances(1:length(vel)), vel,'+-');
    hold on
    
    pause(0.1)
    figure(nb_shift_fig_for_rad+nb_fig)
    pause(0.1)
    plot(record_distances(1:length(rad)), rad,'+-');
    hold on
end

%%
pause(0.1)
figure(nb_fig)
pause(0.1)

xlabel('Record distance (meters)','interpreter','latex')
ylabel('Mean Z speed (fraction of speed of light)','interpreter','latex')

grid on
title(['Mean speed VS distance, ' codename],'interpreter','latex')

legend(the_legend,'Location','southeast','interpreter','latex');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
figname = ['./generated_figures/mean_speed/code_by_code/mean_speed_' particleName '_' codename];
savefig([figname '.fig'])
print(figname,'-depsc')

%%
pause(0.1)
figure(nb_shift_fig_for_rad+nb_fig)
pause(0.1)

xlabel('Record distance (meters)','interpreter','latex')
ylabel('Mean XY radius (meters)','interpreter','latex')

grid on
title(['Mean XY radius VS distance, ' codename],'interpreter','latex')

legend(the_legend,'Location','southeast','interpreter','latex');
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
figname = ['./generated_figures/mean_XY_radius/code_by_code/mean_XYradius_' particleName '_' codename];
savefig([figname '.fig'])
print(figname,'-depsc')


clear HEAP_data_RREA;

end



%%

function plot_comparison()
global nb_fields_save;
global velocity;
global radius;
global record_distances_save;

disp_names = {'GRRR','REAM','GEANT4O1','GEANT4O4'};

fields = [6:2:28]*1e5 ;


%% radii
for i_field = 1:length(fields)
    
    field = fields(i_field);
    
    pause(0.1)
    figure(100+i_field)
    pause(0.1)
    
    for i_model = 1:4
        
        rad = radius(i_model,i_field,:);
        rad = rad(:);
        
        if (sum(rad)==0)
            plot(NaN,NaN,'DisplayName',disp_names{i_model})
        else
            plot(record_distances_save{i_model},rad,'+-','DisplayName',disp_names{i_model})
        end
        
        hold on
    end
    
    legend('Location','southeast')
    title(['Electrons, E-field of $' num2str(fields(i_field)/1e5) '.10^5$ V/m'],'interpreter','latex')
    grid on
    xlabel('Record distance (meters)','interpreter','latex')
    ylabel('Mean XY radius (meters)','interpreter','latex')
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    figname = ['./generated_figures/mean_XY_radius/code_comparison/mean_XYradius_electron_compa_' num2str(field/1e5) 'e5'];
    savefig([figname '.fig'])
    print(figname,'-depsc')
end



%% speeds
for i_field = 1:length(fields)
    
    field = fields(i_field);
    
    pause(0.1)
    figure(200+i_field)
    pause(0.1)
    
    for i_model = 1:4
        %         if (i_field>(nb_fields_save(i_model)))
        %             continue
        %         end
        vel = velocity(i_model,i_field,:);
        vel = vel(:);
        
        if (sum(vel)==0)
            plot(NaN,NaN,'DisplayName',disp_names{i_model})
        else
            plot(record_distances_save{i_model},vel,'+-','DisplayName',disp_names{i_model})
        end
        hold on
    end
    
    grid on
    xlabel('Record distance (meters)','interpreter','latex')
    ylabel('Mean Z speed (fraction of speed of light)','interpreter','latex')
    
    legend('Location','southeast')
    
    title(['Electrons, E-field of $' num2str(fields(i_field)/1e5) '.10^5$ V/m'],'interpreter','latex')
    
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    figname = ['./generated_figures/mean_speed/code_comparison/mean_speed_electron_compa_' num2str(field/1e5) 'e5'];
    savefig([figname '.fig'])
    print(figname,'-depsc')
end




end

