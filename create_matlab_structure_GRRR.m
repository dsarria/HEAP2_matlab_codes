clear all
close all
clc



%% parameters

c_light =  299792458      ;
m_elec  =  9.10938356e-31 ;
q_elec  =  1.60217662e-19 ;


fields = [6:2:30]*1e5 ;

record_times = [14.,  26.,  39.,  51.,  64.,  78.,  90.,  102.,  116.,  124.,  135., ... % 11
    144.,  153.,  164., 179.,  199.,  215.,  233.,  262.,   277.,  290.,  312., ... % 11
    351.,  406.,  464.,  479., 522.,  599.,  719.,  838.,  958., 1078.] ; % 10

record_distances = [4.,  7.,  10.,   14.,  17.,   21.,   24.,  27.,  31.,  33.,  36., ... % 11
    38., 41.,  44.,  48.,  53.,  57.,  62.,  70.,  74.,  77.,  83., ... % 11
    94., 108., 124.,  128.,  139.,  160.,  192.,  224.,  256.,  288.]   ; % 10

stop_times = [1078,  522, 351, 277,  233,  199,  179,   164,  153,  144,  135, 130,   124] ;



% codenames_list = {'GEANT4O4','GEANT4O1'};
% codenames_list = {'GEANT4O4','GEANT4O1','GRRR','REAM'};


codename{1} = 'GRRR';

%% creating MATLAB data structure from text files


% /home/sarria/Desktop/HEAP/fused_RREA
% folder = '/scratch/output/fused_RREA/';

% folder{1} = '/scratch/ARAGO_workdir/HEAP/data/output_RREA_CHAR_G4_2/O1/';
% folder{end+1} = '/scratch/ARAGO_workdir/HEAP/data/output_RREA_CHAR_G4_2/O4/';
folder{1} = '/scratch/HEAP_data/GRRR2/dist_and_time/';
% folder{end+1} = '/scratch/ARAGO_workdir/HEAP/data/output_RREA_CHAR_REAM2';


listing = dir(folder{1});

filenames = {};

for i=3:length(listing)
    filename = listing(i).name;
    filenames{i-2} = char(filename);
end

nb_ini = [200 200 200 200 200 200 200 20 20 20 20 20 5];


for i_field = 1:length(fields)
    
    field = fields(i_field);
    
    for filename = filenames
        
        full_file_path = [folder{1} filename{1}];
        
        field_string = get_field_string(field);
        
        if (  contains(full_file_path, field_string) && contains(full_file_path, codename{1}) )
            if exist(full_file_path, 'file') == 2

                
                fid = fopen(full_file_path);
                C = textscan(fid,'%f %f %f %f %f %f %f %f','CommentStyle','#');
                data_temp = [C{:}];
                
                type = data_temp(:,1);
                
                time = data_temp(:,8)*1e9; % s to ns
                px = data_temp(:,5);
                py = data_temp(:,6);
                pz = data_temp(:,7);
                ener = get_ener_REAM_data(type,px,py,pz); % getting energies in keV
                zz = data_temp(:,4);
                yy = data_temp(:,3);
                xx = data_temp(:,2);
                
                % writing inside structure
                
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).nb_seed = nb_ini(i_field);
                
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.energy = ener(type==-1); % keV
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.energy = ener(type==1);
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.energy   = ener(type==0);
                
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.z = zz(type==-1); % meters
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.z = zz(type==1);
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.z   = zz(type==0);
                
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.y = yy(type==-1); % meters
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.y = yy(type==1);
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.y   = yy(type==0);
                %
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.x = xx(type==-1); % meters
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.x = xx(type==1);
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.x   = xx(type==0);
                
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).electron.time = time(type==-1); % ns
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).positron.time = time(type==1) ; % ns
                HEAP_data_RREA.(codename{1}).(['field_' num2str(field)]).photon.time   = time(type==0) ; % ns
                
            end
            
        end
        
    end
end



% additional information

HEAP_data_RREA.stop_times = stop_times ;

HEAP_data_RREA.record_times = record_times;

HEAP_data_RREA.record_distances = record_distances;

HEAP_data_RREA.field_list = fields;

HEAP_data_RREA.nb_fields = 7;

HEAP_data_RREA.codenames_list = {'G4O1','G4O4','REAM','GRRR'};




%%


function field_string = get_field_string(field)
%% adds zeros at the begining of a string so that it has 8 characters in total
field_string = num2str( round(field) );

nb_to_add = 8 - length(field_string);

if (nb_to_add>=0)
    for ii=1:nb_to_add
        field_string = ['0' field_string] ;
    end
end

field_string = ['_' field_string '.txt'];

end

%%

function eners = get_ener_REAM_data(type,px,py,pz)

elec_mass = 9.10938356e-31;
c_light = 2.99792458e8;

mass = type;
mass(type==0) = 0 ;
mass(type~=0) = elec_mass;

p2 = px.^2 + py.^2 + pz.^2;

eners = sqrt(p2 * c_light.^2 + mass.^2 * c_light^4) - mass * c_light.^2; % SI units

eners = eners / 1.60217662e-19 /1000 ; % from joules to keV

end

