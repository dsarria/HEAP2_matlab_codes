clear all
close all
clc

%% globals

global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';

%% plot for all codes

plot_mul_fact_and_ava_times('GRRR')
plot_mul_fact_and_ava_times('REAM')
plot_mul_fact_and_ava_times('GEANT4O1')
plot_mul_fact_and_ava_times('GEANT4O4')


%%

function plot_mul_fact_and_ava_times(codename)
global path_to_processed_files;

fields = [6:2:28]*1e5 ;

load([path_to_processed_files codename '_data.mat']);

the_legend={};

for ii=1:length(fields)
    
    the_legend{end+1} = ['E-field of ' num2str(fields(ii)/1e5) '.$10^5$ V/m'];
    
    field = fields(ii);
    
    distances = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.z;
    nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
    
    record_distances = HEAP_data_RREA.record_distances;
    
    for jj = 1:length(record_distances)
        mult_fact(jj) = sum(distances>=record_distances(jj)*0.999 & distances<=record_distances(jj)*1.001) / nb_seed;
    end
    
    mult_fact = filter(mult_fact,ii,codename);
    
    pause(0.1)
    figure(ii)
    pause(0.1)
    
    semilogy(record_distances,mult_fact,'-+','DisplayName',codename)
    hold on
    
    xlabel('Distance (m)','interpreter','latex')
    ylabel('Multiplciation factor','interpreter','latex')
    grid on
    title(['E-field of ' num2str(fields(ii)/1e5) '.$10^5$ V/m'],'interpreter','latex')
    
    legend('show')
    legend('Location','southeast');
    
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    figname = ['./generated_figures/multiplication_factors/distance_record_code_comparison/mult_fact_distance_compa_' num2str(fields(ii)/1e5) 'e5'];
    savefig([figname '.fig'])
    print(figname,'-depsc')
    
end

clear HEAP_data_RREA;

end


% to remove some of the last value that do not follow the general slope
% (attributed to electrons produced by photons interaction at a long distance)
function mult_fact2 = filter(mult_fact,i_field,codename)


mult_fact2 = mult_fact;

if contains(codename,'GRRR')
    map = [1 1 1 2 1 2 2 2 2 2 2 3];
elseif contains(codename,'REAM')
    map = [0 2 1 3 1 2 2 2 2 2 2 2];
elseif contains(codename,'GEANT4O1')
    map = [1 2 1 3 1 2 2 2 2 2 2 2];
elseif contains(codename,'GEANT4O4')
    map = [1 2 1 3 2 2 2 2 2 2 2 2];
end


nb_to_remove = map(i_field);

nb_done=0;

for ii = length(mult_fact2):-1:1
    
    if nb_done == nb_to_remove
        break;
    end
    
    if mult_fact2(ii)==0
        continue
    else
        mult_fact2(ii)=0;
        nb_done = nb_done + 1;
    end
    
    if nb_done == nb_to_remove
        break;
    end
end


end











% %% comparing all codes
%
% figure
% plot(GEANT4O1.mean_ener.field_value, GEANT4O1.mul_fact.avalanche_times,'-+')
% hold on
% plot(GEANT4O4.mean_ener.field_value, GEANT4O4.mul_fact.avalanche_times,'-+')
% plot(REAM.mean_ener.field_value, REAM.mul_fact.avalanche_times,'-+')
% plot(GRRR.mean_ener.field_value(1:length(GRRR.mul_fact.avalanche_times)), GRRR.mul_fact.avalanche_times,'-+')
%
% grid on
% xlabel('Electric field (V/m)')
% ylabel('Avalanche time (ns)')
%
% legend('GEANT4O1','GEANT4O4','REAM','GRRR')
%
%
% %% functions
%
% function index = find_index(Efield, Efield_list)
%
% for ii=1:length(Efield_list)
%     if(Efield == Efield_list(ii))
%         index = ii;
%     end
% end
%
% end


