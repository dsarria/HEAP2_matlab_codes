clearvars
close all
clc

set(0,'defaultAxesFontSize',12)

global ss_speed;
ss_speed=[];

global velocity;
global radius;
velocity=[];
radius=[];

global nb_fields_save;
nb_fields_save = [];
global record_distances_save;
record_distances_save={};

global nb_fig;
nb_fig=0;
global colors;
colors={'r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m'};

global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';


%% G4O1

plot_mul_fact_and_ava_times('GRRR');
plot_mul_fact_and_ava_times('REAM');
plot_mul_fact_and_ava_times('GEANT4O1');
plot_mul_fact_and_ava_times('GEANT4O4');

plot_comparison();



%%

function plot_mul_fact_and_ava_times(codename)
global path_to_processed_files;
global nb_fig;
global velocity;
global nb_fields_save;
global record_distances_save;
global ss_speed;


if contains(codename,'GRRR')
    map = [1 1 1 2 1 2 2 2 2 2 2 3];
    i_code=1;
elseif contains(codename,'REAM')
    map = [0 2 1 3 1 2 2 2 2 2 2 2];
    i_code=2;
elseif contains(codename,'GEANT4O1')
    map = [1 2 1 3 1 2 2 2 2 2 2 2 2];
    i_code=3;
elseif contains(codename,'GEANT4O4')
    map = [1 2 1 3 2 2 2 2 2 2 2 2];
    i_code=4;
end


c_light = 2.99792458e8;

nb_fig = nb_fig + 1;

% nb_fig is also code number

particleName='electron';

load([path_to_processed_files codename '_data.mat']);

record_distances = HEAP_data_RREA.record_distances;

record_distances_save{nb_fig} = HEAP_data_RREA.record_distances;

fields = HEAP_data_RREA.field_list;

the_legend={};

nb_fields_save(nb_fig) = HEAP_data_RREA.nb_fields;

for i_field=1:12
    
    the_legend{end+1} = ['E-field of $' num2str(fields(i_field)/1e5) '.10^5$ V/m'];
    
    field = fields(i_field);
    
    xx = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.x;
    yy = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.y;
    zz = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.z;
    time = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).time;
    
    for i_dist = 1:length(record_distances)
        
        dist = record_distances(i_dist);
        
        to_be_kept = (zz >= 0.999*dist & zz <= 1.001*dist);
        
        time_t = time(to_be_kept);
        
        velocity(nb_fig,i_field,i_dist) = dist / mean(time_t*1e-9) / c_light;
        
    end
    
    nb_to_delete = map(i_field);
    
    vel = velocity(nb_fig,i_field,:);
    vel = vel(:);
    
    vel(isnan(vel)) = [];
    
    vel(end-nb_to_delete:end) = [];
    
    ss_speed(i_code, i_field) = mean(vel(end-2:end));
    
    pause(0.1)
    figure(nb_fig)
    pause(0.1)
    
    plot(record_distances(1:length(vel)), vel,'+-');
    hold on
    
    
    
end

%%
pause(0.1)
figure(nb_fig)
pause(0.1)

xlabel('Record distance (meters)','interpreter','latex')
ylabel('Mean Z speed (fraction of speed of light)','interpreter','latex')

grid on
title(['Mean speed VS distance, ' codename],'interpreter','latex')

legend(the_legend,'Location','southeast','interpreter','latex');

set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
figname = ['./generated_figures/mean_speed/code_by_code/mean_speed_' particleName '_' codename];
savefig([figname '.fig'])
print(figname,'-depsc')

%%
clear HEAP_data_RREA;

end



%%

function plot_comparison()
global record_distances_save;
global ss_speed;

disp_names = {'GRRR','REAM','GEANT4O1','GEANT4O4'};

fields = [6:2:28]*1e5 ;

%% speeds
for i_model = 1:4
    figure(200)
    
    to_plot_field = fields;
    to_plot_speed = ss_speed(i_model, :);
    to_plot_field(to_plot_speed==0)=[];
    to_plot_speed(to_plot_speed==0)=[];
    
    plot(to_plot_field,to_plot_speed,'+-','DisplayName',disp_names{i_model})
    hold on
    
    grid on
    xlabel('Electric field (V/m)','interpreter','latex')
    ylabel('Z speed at steady state (fraction of speed of light)','interpreter','latex')
    
    legend('Location','southeast')
    
    title(['steady state electron speed comparison'],'interpreter','latex')
    
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    figname = ['./generated_figures/mean_speed/steady_state_speed_code_comparison/steady_state_speed_electron_compa'];
    savefig([figname '.fig'])
    print(figname,'-depsc')
    
end



end

