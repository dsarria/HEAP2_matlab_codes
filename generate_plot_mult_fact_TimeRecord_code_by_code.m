clear all
close all
clc


set(0,'defaultAxesFontSize',12)

%% globals

global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';


%% plot for all codes

plot_mul_fact_and_ava_times('GRRR')
plot_mul_fact_and_ava_times('REAM')
plot_mul_fact_and_ava_times('GEANT4O1')
plot_mul_fact_and_ava_times('GEANT4O4')


%%

function plot_mul_fact_and_ava_times(codename)
global path_to_processed_files;


fields = [6:2:28]*1e5 ;

load([path_to_processed_files codename '_data.mat']);

the_legend={};

for ii=1:length(fields)
    
    field = fields(ii);
    
    times = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.time;
    nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
    
    record_times = HEAP_data_RREA.record_times;
    
    for jj = 1:length(record_times)
        mult_fact(jj) = sum(times>=record_times(jj)*0.999 & times<=record_times(jj)*1.001) / nb_seed;
    end
    
    pause(0.1)
    figure(ii)
    pause(0.1)
    
    ppp = plot(record_times,mult_fact,'DisplayName',codename);
    ppp.Marker = '+';
    set(gca, 'YScale', 'log')
    hold on
    
    record_times(mult_fact==0)=[];
    mult_fact(mult_fact==0)=[];
    
    xlabel('Time (ns)','interpreter','latex')
    ylabel('Multiplciation factor','interpreter','latex')
    grid on
    title(['E-field of ' num2str(fields(ii)/1e5) '.$10^5$ V/m'],'interpreter','latex')
    legend('show');
    legend('Location','southeast');
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    
    figname=['./generated_figures/multiplication_factors/time_record_code_comparison/mult_fact_time_' num2str(fields(ii)/1e5) 'e5'];
    
    savefig([figname '.fig'])
    print(figname,'-depsc')
    
end


clear HEAP_data_RREA;

end

