clear all
close all
clc


set(0,'defaultAxesFontSize',12)

%% globals
global nb_fig;
nb_fig = 0;
global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';
global bins;
global bins2;
bins = logspace(log10(10),log10(80000),120);
bins2 = logspace(log10(10),log10(80000),30);

global corr_dist;
corr_dist = [256 128 83 77 57 53 48 44 41 38 36 33 33] ;

global the_legend;

% electrons
% the_legend.field{1,1} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,2} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,3} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,4} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,5} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,6} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,7} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,8} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,9} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,10} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,11} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,12} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% % the_legend.field{1,13} = {'GEANT4O1'};

% photons

% the_legend.field{2,1} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,2} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,3} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,4} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,5} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,6} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,7} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,8} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,9} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,10} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,11} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,12} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,13} = {'GEANT4O1'};


%% plot for all codes

% plot_spectra('GRRR','electron')
% plot_spectra('REAM','electron')
% plot_spectra('GEANT4O1','electron')
% plot_spectra('GEANT4O4','electron')

% % GRRR do not have photons and positrons
plot_spectra('REAM','photon')
plot_spectra('GEANT4O1','photon')
plot_spectra('GEANT4O4','photon')

plot_spectra('REAM','positron')
plot_spectra('GEANT4O1','positron')
plot_spectra('GEANT4O4','positron')

%%

function plot_spectra(codename,particleName)
global path_to_processed_files;
global nb_fig;
global bins;
global bins2;
% global the_legend;
global corr_dist;

if contains(particleName,'electron')
    ipart=1;
    binss = bins;
    fig_nb0=0;
elseif contains(particleName,'photon')
    ipart=2;
    binss = bins;
    fig_nb0=14;
elseif contains(particleName,'positron')
    ipart=2;
    binss = bins2;
    fig_nb0=28;
end

fields = [6:2:30]*1e5 ;

nb_fig = nb_fig +1;

load([path_to_processed_files codename '_data.mat']);


for ii=1:12
    
        figure(fig_nb0+ii)
        
        field = fields(ii);
        
        dist = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).z;
        energies = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).energy;
        nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
        
        stop_dist = corr_dist(ii);
        
        energies = energies(dist >= stop_dist*0.999 & dist <= stop_dist*1.001);
        [nn3,xout3] = histcounts(energies,binss);
        nn3 = nn3 ./diff(xout3) ./ nb_seed;
        nn3 = nn3 ./sum(nn3);
        
        nn3(isnan(nn3)) = 0;
        
        if sum(nn3)==0 || isnan(sum(nn3))
           plot([0],[0],'DisplayName',codename);
           hold on
           continue; 
        end
        
        histogram('BinEdges',xout3,'BinCounts',nn3,'DisplayStyle','stairs','DisplayName',codename)
        hold on
        
        xlabel('Energy (keV)','interpreter','latex')
        ylabel('counts per bin, per bin size (keV$^{-1}$), per detected particle','interpreter','latex')
        title([particleName ' spectra, E-field of ' num2str(field/1e5) '.$10^5$ V/m ; recorded at ' num2str(stop_dist) ' m'],'interpreter','latex')
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        grid on
        
        %     legend(the_legend.field{ipart,ii})
        legend('show')
        
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
        figname = ['./generated_figures/spectra_comparison/distance_record/spectra_compa_' particleName '_distOfStopTime_' num2str(field/1e5) 'e5'];
        if (field ~= 30e5)
            pause(0.1)
            savefig([figname '.fig'])
            pause(0.1)
            print(figname,'-depsc')
            pause(0.1)
        end
end



clear HEAP_data_RREA;

end

