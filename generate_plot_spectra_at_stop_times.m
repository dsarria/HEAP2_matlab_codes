clear all
close all
clc


set(0,'defaultAxesFontSize',12)

%% globals
global nb_fig;
nb_fig = 0;
global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';
global bins;
global bins2;
bins = logspace(log10(10),log10(80000),120);
bins2 = logspace(log10(10),log10(80000),30);

% global the_legend;

% electrons
% the_legend.field{1,1} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,2} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,3} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,4} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,5} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,6} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,7} = {'GRRR','REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,8} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,9} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,10} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,11} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,12} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{1,13} = {'GEANT4O1'};

% photons

% the_legend.field{2,1} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,2} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,3} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,4} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,5} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,6} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,7} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,8} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,9} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,10} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,11} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,12} = {'REAM','GEANT4O1','GEANT4O4'};
% the_legend.field{2,13} = {'GEANT4O1'};


%% plot for all codes

plot_mul_fact_and_ava_times('GRRR','electron')
plot_mul_fact_and_ava_times('REAM','electron')
plot_mul_fact_and_ava_times('GEANT4O1','electron')
plot_mul_fact_and_ava_times('GEANT4O4','electron')

% % GRRR do not have photons and positrons
plot_mul_fact_and_ava_times('REAM','photon')
plot_mul_fact_and_ava_times('GEANT4O1','photon')
plot_mul_fact_and_ava_times('GEANT4O4','photon')

plot_mul_fact_and_ava_times('REAM','positron')
plot_mul_fact_and_ava_times('GEANT4O1','positron')
plot_mul_fact_and_ava_times('GEANT4O4','positron')






%%

function plot_mul_fact_and_ava_times(codename,particleName)
global path_to_processed_files;
global nb_fig;
global bins;
global bins2;
global the_legend;

if contains(particleName,'electron')
    ipart=1;
    binss = bins;
    fig_nb0=0;
elseif contains(particleName,'photon')
    ipart=2;
    binss = bins;
    fig_nb0=14;
elseif contains(particleName,'positron')
    ipart=2;
    binss = bins2;
    fig_nb0=28;
end

fields = [6:2:30]*1e5 ;

nb_fig = nb_fig +1;

load([path_to_processed_files codename '_data.mat']);



stop_times = HEAP_data_RREA.stop_times;

for ii=1:HEAP_data_RREA.nb_fields
    try
        pause(0.1)
        figure(fig_nb0 + ii)
        pause(0.1)
        
        field = fields(ii);
        
        time = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).time;
        energies = HEAP_data_RREA.(codename).(['field_' num2str(field)]).(particleName).energy;
        nb_seed = HEAP_data_RREA.(codename).(['field_' num2str(field)]).nb_seed;
        
        stop_tim = stop_times(ii);
        
        energies = energies(time >= stop_tim*0.999 & time <= stop_tim*1.001);
        [nn3,xout3] = histcounts(energies,binss);
        nn3 = nn3 ./diff(xout3) ./ nb_seed;
        nn3 = nn3./sum(nn3);
        
        histogram('BinEdges',xout3,'BinCounts',nn3,'DisplayStyle','stairs','DisplayName',codename)
        hold on
        
        xlabel('Energy (keV)','interpreter','latex')
        ylabel('counts per bin, per bin size (keV$^{-1}$), per detected particle','interpreter','latex')
        title([particleName ' spectra, E-field of ' num2str(field/1e5) '.$10^5$ V/m ; recorded at ' num2str(stop_tim) ' ns'],'interpreter','latex')
        set(gca, 'YScale', 'log')
        set(gca, 'XScale', 'log')
        grid on
        
        %     legend(the_legend.field{ipart,ii})
        legend('show')
        
        set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
        
        figname = ['./generated_figures/spectra_comparison/time_record/spectra_compa_' particleName '_stopTimes_' num2str(field/1e5) 'e5'];
        pause(0.1)
        savefig([figname '.fig'])
        pause(0.1)
        print(figname,'-depsc')
        pause(0.1)
    end
end



clear HEAP_data_RREA;

end











% %% comparing all codes
%
% figure
% plot(GEANT4O1.mean_ener.field_value, GEANT4O1.mul_fact.avalanche_times,'-+')
% hold on
% plot(GEANT4O4.mean_ener.field_value, GEANT4O4.mul_fact.avalanche_times,'-+')
% plot(REAM.mean_ener.field_value, REAM.mul_fact.avalanche_times,'-+')
% plot(GRRR.mean_ener.field_value(1:length(GRRR.mul_fact.avalanche_times)), GRRR.mul_fact.avalanche_times,'-+')
%
% grid on
% xlabel('Electric field (V/m)')
% ylabel('Avalanche time (ns)')
%
% legend('GEANT4O1','GEANT4O4','REAM','GRRR')
%
%
% %% functions
%
% function index = find_index(Efield, Efield_list)
%
% for ii=1:length(Efield_list)
%     if(Efield == Efield_list(ii))
%         index = ii;
%     end
% end
%
% end


