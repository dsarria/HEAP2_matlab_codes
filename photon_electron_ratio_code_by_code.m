clearvars
close all
clc

set(0,'defaultAxesFontSize',12)

global corr_dist;
corr_dist = [256 128 83 77 57 53 48 44 41 38 36 33 33] ;


global nb_fig;
nb_fig=0;
global colors;
colors={'r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m','r','g','b','k','m'};

global path_to_processed_files;
path_to_processed_files = '/scratch/HEAP_data/matlab_structs/';


load([path_to_processed_files 'GRRR' '_data.mat']);

%%

plot_elec_phot_ratio('REAM');
plot_elec_phot_ratio('GEANT4O1');
plot_elec_phot_ratio('GEANT4O4');



%%

function plot_elec_phot_ratio(codename)
global path_to_processed_files;
global corr_dist;

the_legend={};

removing_map = [0 1 0 3 0 1 1 1 1 1 1 0 1];

load([path_to_processed_files codename '_data.mat']);

fields = HEAP_data_RREA.field_list;

stop_times = HEAP_data_RREA.stop_times;
% record_times = HEAP_data_RREA.record_times;
record_distances = HEAP_data_RREA.record_distances;
stop_dists = corr_dist;

figure

for i_field=1:HEAP_data_RREA.nb_fields
    
    record_distances = record_distances(record_distances <= stop_dists(i_field));
    
    field = fields(i_field);
    
    %     ener_elec = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.energy;
    %     ener_phot = HEAP_data_RREA.(codename).(['field_' num2str(field)]).photon.energy;
    
    dist_elec = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.z;
    dist_phot = HEAP_data_RREA.(codename).(['field_' num2str(field)]).photon.z;
    
    time_phot = HEAP_data_RREA.(codename).(['field_' num2str(field)]).photon.time*1e-9;
    time_elec = HEAP_data_RREA.(codename).(['field_' num2str(field)]).electron.time*1e-9;
    
    ratio=[];
    nb_elec=[];
    nb_phot=[];
    
    stop_time = stop_times(i_field);
    
    the_legend{end+1} = ['E-field of ' num2str(fields(i_field)/1e5) '.$10^5$ V/m'];
    
    for i_dist = 1:length(record_distances)
        
        rec_dist = record_distances(i_dist);
        
        to_be_kept_elec = (dist_elec >= 0.999*rec_dist & dist_elec <= 1.001*rec_dist );
        to_be_kept_phot = (dist_phot >= 0.999*rec_dist & dist_phot <= 1.001*rec_dist );
%       to_be_kept_elec = (dist_elec >= 0.999*rec_dist & dist_elec <= 1.001*rec_dist & time_elec <= stop_time);
%       to_be_kept_phot = (dist_phot >= 0.999*rec_dist & dist_phot <= 1.001*rec_dist & time_phot <= stop_time);
        
        nb_elec(i_dist) = sum(to_be_kept_elec);
        nb_phot(i_dist) = sum(to_be_kept_phot);
        
        ratio(i_dist) = nb_elec(i_dist) / nb_phot(i_dist);
        
    end
    
    nb_to_remove = removing_map(i_field);
    
    record_distances = record_distances(1:end-nb_to_remove);
    
    ratio = ratio(1:end-nb_to_remove);
    
    plot(record_distances,ratio,'+-')
    
    hold on

end


    xlabel('Record distance (meter)','interpreter','latex')
    ylabel('Electron to photon ratio','interpreter','latex')
    
    grid on
    
    legend(the_legend,'Location','northeast','interpreter','latex')
    
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
    
    title([codename ', Electron to Photon ratio'],'interpreter','latex')
    
    figname = ['./generated_figures/electron_to_photon_ratio/code_by_code/phot_elec_ratio_' codename];
    savefig([figname '.fig'])
    print(figname,'-depsc')


clear HEAP_data_RREA;

end
